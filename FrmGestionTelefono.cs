﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria030919
{
    public partial class FrmGestionTelefono : Form
    {
        DataSet dsTelefonos;
        BindingSource bsTelefonos;

        public FrmGestionTelefono()
        {
            InitializeComponent();
            bsTelefonos = new BindingSource();
        }

        public DataSet DsTelefonos
        {
            set
            {
                dsTelefonos = value;
            }
        }

        private void FrmGestionTelefono_Load(object sender, EventArgs e)
        {
            bsTelefonos.DataSource = dsTelefonos;
            bsTelefonos.DataMember = dsTelefonos.Tables["Telefono"].TableName;
            dgvTelefonos.DataSource = bsTelefonos;
            dgvTelefonos.AutoGenerateColumns = true;
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            FrmTelefono ft = new FrmTelefono();
            ft.DtTelefono = dsTelefonos.Tables["Telefono"];
            ft.Show();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection seleccionados = dgvTelefonos.SelectedRows;

            if(seleccionados.Count == 0)
            {
                MessageBox.Show(this, "Para editar un teléfono, primero debe seleccionar uno.",
                    "Mensaje de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow fila = seleccionados[0];
            DataRow edita = ((DataRowView)fila.DataBoundItem).Row;

            FrmTelefono ft = new FrmTelefono();
            ft.DrEdita = edita;
            ft.DtTelefono = dsTelefonos.Tables["Telefono"];
            ft.Show();
        }
    }
}
