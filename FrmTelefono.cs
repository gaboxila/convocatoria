﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria030919
{
    public partial class FrmTelefono : Form
    {
        DataTable dtTelefono;
        DataRow drEdita;

        public FrmTelefono()
        {
            InitializeComponent();
        }

        public DataTable DtTelefono { get => dtTelefono; set => dtTelefono = value; }
        public DataRow DrEdita
        {
            set
            {
                drEdita = value;
                txtId.Text = drEdita["Id"].ToString();
                txtCantidad.Text = drEdita["Cantidad"].ToString();
                txtMarca.Text = drEdita["Marca"].ToString();
                txtModelo.Text = drEdita["Modelo"].ToString();
                cmbSistema.Text = drEdita["Sistema"].ToString();
                cmbTamaño.Text = drEdita["Tamaño"].ToString();
                cmbPantalla.Text = drEdita["Pantalla"].ToString();
                cmbMemoria.Text = drEdita["Memoria"].ToString();
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            string marca, modelo, fecha, sistema, tamaño, pantalla, memoria;
            int cantidad;

            string id = txtId.Text;
            marca = txtMarca.Text;
            modelo = txtModelo.Text;
            fecha = dateFecha.Text;
            sistema = cmbSistema.SelectedItem.ToString();
            tamaño = cmbTamaño.SelectedItem.ToString();
            pantalla = cmbPantalla.SelectedItem.ToString();
            memoria = cmbMemoria.SelectedItem.ToString();
            cantidad = int.Parse(txtCantidad.Text);

            if (drEdita != null)
            {
                DataRow drNuevo = dtTelefono.NewRow();
                int indice = DtTelefono.Rows.IndexOf(drEdita);
                drNuevo["Id"] = drEdita["Id"];
                drNuevo["Marca"] = marca;
                drNuevo["Modelo"] = modelo;
                drNuevo["Fecha"] = fecha;
                drNuevo["Sistema"] = sistema;
                drNuevo["Tamaño"] = tamaño;
                drNuevo["Pantalla"] = pantalla;
                drNuevo["Memoria"] = memoria;
                drNuevo["Cantidad"] = cantidad;
                DtTelefono.Rows.RemoveAt(indice);
                DtTelefono.Rows.InsertAt(drNuevo, indice);
                DtTelefono.Rows[indice].AcceptChanges();
                DtTelefono.Rows[indice].SetModified();
            }
            else
            {
                dtTelefono.Rows.Add(marca, modelo, fecha, sistema, tamaño, pantalla,
                    memoria, id, cantidad);
            }
            Dispose();
        }

        private void FrmTelefono_Load_1(object sender, EventArgs e)
        {
            if(drEdita == null)
            {
                txtId.Text = "TE" + (dtTelefono.Rows.Count + 1);
            }
        }
    }
}
