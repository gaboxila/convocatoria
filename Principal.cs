﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convocatoria030919
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void CatálogoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionTelefono fgt = new FrmGestionTelefono();
            fgt.MdiParent = this;
            fgt.DsTelefonos = dsSistema;
            fgt.Show();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            MessageBox.Show(this, "El programa trabaja con datos dinámicos guardados dentro de los " +
                "DataSet", "Mensaje de información", MessageBoxButtons.OK);
        }
    }
}
